# Changelog

## isp.001-rc5

* Fixed Android getAllCellInfo API error handling.

* Updated libevent dependency to version 2.1.12.

* Added support for TR-69 UPLOAD RPC using SMTP protocol.

* Fixed log session JNI call while running, after closing the host application.

## isp.001-rc4

* Fixed the printInitialCPEInfo call by using the right settings object as argument.

## isp.001-rc3

* Added support for TCP Echo diagnostics.

* UDP Echo diagnostic code refactor.

* Added support for changing messages logging level through a datamodel parameter.

* Added MacAddress information for Android 8 devices and higher. But be aware that this is not the real information.

## isp.001-rc2

* Improve Android public API initialization access validation.

* Added datamodel parameters to allow ACS publishing its final state/result after finishing a scenario.

* Datamodel internal refactoring.

* Fixed the message logging level initialization.

* Changing the Android APP_PLAFTORM to android-21, to fix Android 6 intialization problems.

* Fix the LAC filter to be applied only when a valid value is read.

* Added support to retrieve SimCard information for Android 5.1 devices (API 21).

* Fixed the Point Of Test discovery diagnostic. It wasn't being scheduled as an internal user task.

## isp.001-rc1

* Support for different release flavors.

* Support for XMPP protocol for ConnectionRequest using a native implementation. 
