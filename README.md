# Axmobility (Staging version)

AxMobility is the Axiros's solution to enable devices perform voice, data and OTT services
quality diagnostics.

## API

The library documentation can be found [here](https://gitlab.com/axiros-latam/axmobility-android-staging/-/wikis/home).
